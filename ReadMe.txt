How to use SVM:

1) Run preprocess code (all the code before "Algorithme 1 - SVM").
2) Run the code in "Algorithme 1 - SVM" section. The result (accuracy) will be printed as output. To change the error penalty, change the value of C in the svm.SVC() function. 

How to use the MLP:
1)On google colab, you can upload the training images and labels, or use the kaggle api to download the files, to use the kaggle api in colab you have to place the file containing the api keys in your google drive. 
2)Run all the cells

How to use the CNN:
1)On google colab, you can upload the training images and labels, or use the kaggle api to download the files, to use the kaggle api in colab you have to place the file containing the api keys in your google drive. 
2)Run all the cells

